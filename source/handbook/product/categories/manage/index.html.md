---
layout: markdown_page
title: "Manage Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Manage
{: #welcome}

The responsibilities of this team are described by the [Manage product
category](/handbook/product/categories/#manage). Among other things, this means 
working on GitLab's functionality around user, group and project administration, 
authentication, access control, and subscriptions.

* I have a question. Who do I ask?

In GitLab issues, questions should start by @ mentioning the Product Manager for the [Manage product
category](/handbook/product/categories/#dev). GitLabbers can also use [#g_manage](https://gitlab.slack.com/messages/CBFCUM0RX).

### How we work

* In accordance with our [GitLab values](https://about.gitlab.com/handbook/values/)
* Transparently: nearly everything is public, we record/livestream meetings whenever possible
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos

#### Planning

We plan in monthly cycles in accordance with our [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline). 
Release scope for an upcoming release should be finalized by the `1st`. We use the following timeline:

* On or around the `24th`: Product meets with Engineering Managers for a preliminary issue review. Issues are tagged with a milestone.
* On or around the `28th`: Product takes the reviewed scope to the larger team for discussion. This becomes the release scope for the next release.
* On or around the `6th`: Product meets with Engineering Managers for a pre-kickoff check-in.

#### Prioritization

Our planning process is reflected in boards that should always reflect our current priorities. You can see our priorities for [backend](https://gitlab.com/groups/gitlab-org/-/boards/704240) and [frontend](https://gitlab.com/groups/gitlab-org/-/boards/810974) engineering. Please filter these boards for the relevant milestone.

 Priorities should be reflected in the priority label for scheduled issues:

| Priority | Description | Probability of shipping in milestone | 
| ------ | ------ | ------ |
| P1 | **Urgent**: top priority for achieving in the given milestone. These issues are the most important goals for a release and should be worked on first; some may be time-critical or unblock dependencies. | ~100% |
| P2 | **High**: important issues that have significant positive impact to the business or technical debt. Important, but not time-critical or blocking others.  | ~75% |
| P3 | **Normal**: incremental improvements to existing features. These are important iterations, but deemed non-critical. | ~50% |
| P4 | **Low**: stretch issues that are acceptable to postpone into a future release. | ~25% | 


You can read more about prioritization on the [direction page](https://about.gitlab.com/direction/manage/#how-we-prioritize) for the Manage stage of the DevOps lifecycle.

#### During the release

If a developer has completed work on an issue, they may open the prioritization board for their respective area ([backend](https://gitlab.com/groups/gitlab-org/-/boards/704240) or [frontend](https://gitlab.com/groups/gitlab-org/-/boards/810974))
and begin working on the next prioritized issue. 

Issues not already in development should be worked on in priority order.

#### Retrospectives

After the `8th`, the Manage team conducts an [asynchronous retrospective](https://about.gitlab.com/handbook/engineering/management/team-retrospectives/). You can find current and past retrospectives for Manage in [https://gitlab.com/gl-retrospectives/manage/issues/](https://gitlab.com/gl-retrospectives/manage/issues/).

### Links and resources
{: #links}

* Our Slack channel
  * [#g_manage](https://gitlab.slack.com/messages/CBFCUM0RX)
* Calendar
  * GitLabbers can add [this Manage team calendar](https://calendar.google.com/calendar/b/1?cid=Z2l0bGFiLmNvbV9rOWYyN2lqamExaGoxNzZvbmNuMWU4cXF2a0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) in Google Calendar.
* Meeting agendas
  * Agendas and notes from team meetings can be found in [this Google Doc](https://docs.google.com/document/d/1kE8udlwjAiMjZW4p1yARUPNmBgHYReK4Ks5xOJW6Tdw/edit). For transparency across the team, we use one agenda document.
* Recorded meetings
  * Recordings should be added to YouTube and added to [this playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthZ-D0khZ_NSb5Bdl2xkF97m).
